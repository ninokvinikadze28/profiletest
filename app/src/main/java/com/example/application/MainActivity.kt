package com.example.application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        signInButton.setOnClickListener {
            signIn()
        }
    }
    private fun signIn(){
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        if(email.isNotEmpty() && password.isNotEmpty()){
            signInButton.isClickable = false
            val intent = Intent(this,profileActivity::class.java)
            startActivity(intent)
        }else{
            Toast.makeText(this, "PLEASE FILL ALL FIELDS", Toast.LENGTH_SHORT).show()
        }

    }
}